Requires:
    python3
    matlabplotlib
    pandas
    numpy
    scipy
    seaborn
    wxWidget (to download data)

1. Download air quality data using GUIapi.py

2. Run "python -i process_air_data.py" to start analysing