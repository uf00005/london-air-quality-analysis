import sqlite3
import pandas as pd

# Create the table
def createTable(cursor):
    query = '''
    CREATE TABLE LondonAir(id INTEGER PRIMARY KEY, timestamp TEXT, location TEXT, species TEXT, value REAL)
    '''
    cursor.execute(query)

# Remove table for a location
def dropTable(cursor, location):
    cursor.execute('''DROP TABLE ?''', (location,))

# Insert the data, paramater "data" should be a pandas dataframe containing the timestamps and the readings
def insertData(cursor, data, location, species):
    for row in data.values:
        cursor.execute('''
        INSERT INTO LondonAir(timestamp, value, location, species) VALUES(?, ?, ?, ?)
        ''', tuple(row) + (location, species))

# Get data for a certain species at a location
def getLocationSpeciesData(cursor, location, species):
    cursor.execute('''SELECT timestamp, value FROM LondonAir WHERE location = ? AND species = ?''', (location, species))
    data = pd.DataFrame.from_records(cursor.fetchall(), columns=["MeasurementDateGMT", species])
    return data

# Get all the locations that are of type t
def get_type_locations(cursor, t):
    cursor.execute('''SELECT location FROM LocationTypes WHERE type = ?''', (t,))
    return [l[0] for l in cursor.fetchall()]

# Get all the data for stations of type t
def get_type_data(cursor, t):
    locations = get_type_locations(cursor, t)
    pollutants = ["NO2", "PM10", "PM25"]

    data = {}
    for l in locations:
        data[l] = {}
        for p in pollutants:
            data[l][p] = getLocationSpeciesData(cursor, l, p)

    return data 