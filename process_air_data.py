import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from sklearn.decomposition import NMF
from datetime import datetime
import sqlite3
import db_helper


speciespair = {"Carbon Monoxide (ppm)":"CO",
"Carbon Monoxide (mg/m3)":"COm",
"Nitrogen Dioxide (ppb)":"NO2",
"Nitrogen Dioxide (ug/m3)":"NO2",
"Nitric Oxide (ppb)":"NO",
"Nitric Oxide (ug/m3)":"NOm",
"Oxides of Nitrogen (ppb)":"NOX",
"Oxides of Nitrogen (ug/m3)":"NOXm",
"Ozone (ppb)":"O3",
"Ozone (ug/m3)":"O3",
"PM10 Particulate":"PM10",
"PM10 Particulate (not reference equivalent)":"DUST",
"PM2.5 Particulate":"PM25",
"PM2.5 Particulate (not reference equiv.)":"FINE",
"Sulphur Dioxide (ppb)":"SO2",
"Sulphur Dioxide (ug/m3)":"SO2m",
"Benzene (ppb)":"BENZ",
"Benzene (ug/m3)":"BENZm",
"Barometric Pressure":"BP",
"Rainfall":"RAIN",
"Relative Humidity":"RHUM",
"Solar Radiation":"SOLR",
"Temperature":"TMP",
"Wind Direction":"WDIR",
"Wind Speed":"WSPD"
}

# Get the readings from a pandas DF into a matrix
def extract_species_days(data, species):
    # Clip negative values
    data[species].clip_lower(0, inplace=True)

    days = int(len(data.index)/24)
    r = np.zeros([24,days])
    for i in range(0, days):
        r[:,i] = data[i*24:(i+1)*24][species].values

    # Remove days with missing data
    r = r.T[~np.isnan(r).any(axis=0)].T
    return r

def extract_species_weeks(data, species):
    # Clip negative values
    data[species].clip_lower(0, inplace=True)

    days = [0]*7
    total_days = int(len(data.index)/24)
    total_weeks = int(total_days/7)
    
    for day in range(0, 7):
        r = np.zeros([24, total_weeks])
        for i in range(0, total_weeks):
            start = day*24 + i*24*7
            r[:,i] = data[start:start + 24][species].values
    
        # Remove days with missing data
        r = r.T[~np.isnan(r).any(axis=0)].T
        date = datetime.strptime(data[day*24:day*24+1]["MeasurementDateGMT"].values[0],"%Y-%m-%d %H:%M")
        days[date.weekday()] = r 
    return days

def extract_species_months(data, species):
    month_nums = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
    months = []
    for m in month_nums:
        months.append(extract_species_days(data[data["MeasurementDateGMT"].str[5:7] == m], species))
    
    return months

# With Pandas Dataframe
def plot_seasons_pattern(data, species):
    months = extract_species_months(data, species)
    winter = np.concatenate((months[0], months[1]), axis=1)
    spring = np.concatenate((months[2], months[3], months[4]), axis=1)
    summer = np.concatenate((months[5], months[6], months[7]), axis=1)
    autumn = np.concatenate((months[8], months[9], months[10]), axis=1)
    plt.plot(winter.mean(1))
    plt.plot(spring.mean(1))
    plt.plot(summer.mean(1))
    plt.plot(autumn.mean(1))
    plt.legend(["winter", "spring", "summer", "autumn"])

def find_daily_pattern_nmf(d):
    model = NMF(n_components=1, init='nndsvda')
    W = model.fit_transform(d)
    H = model.components_

    print(model.n_iter_, " iterations")
    return W, H

# With Pandas Dataframe
def plot_weekday_weekend_pattern_nmf(data, species):
    days = extract_species_weeks(data, species)
    weekdays = np.concatenate((days[0], days[1], days[2], days[3], days[4]), axis = 1)
    weekends = np.concatenate((days[5], days[6]), axis = 1)
    
    plot_daily_pattern_nmf_data(weekdays, species)
    plot_daily_pattern_nmf_data(weekends, species)

    plt.legend(["Weekdays", "Weekends"])
    return [weekdays.mean(1), weekends.mean(1)]

# With Pandas Dataframe
def plot_annual_pattern(data, species):
    months = extract_species_months(data, species)

    month_means = []

    for month in months:
        month_means.append(month.mean())

    plt.plot(month_means)
    plt.xticks(np.arange(12), ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'])
    return [month_means]

# With Pandas Dataframe
def plot_weekly_pattern_nmf(data, species):
    days = extract_species_weeks(data, species)
    day_patterns = []

    for day in days:
        W, H = find_daily_pattern_nmf(day)
        day_patterns.append([W.T[0] * H.mean()])

    plots = []

    for day_p in day_patterns:
        plots.append(day_p[0])
        plt.plot(plots[-1])
    
    plt.legend(["Monday", "Tuesday", "Wednesday", "Thurday", "Friday", "Saturday", "Sunday"])
    plt.xlabel("Hours (24h)")
    plt.ylabel(species + " level (ug/m3)")
    return plots

# With Pandas Dataframe
def plot_daily_pattern_nmf(data, species):
    d = extract_species_days(data, species)
    plot_daily_pattern_nmf_data(d, species)

# With raw data
def plot_daily_pattern_nmf_data(data, species):
    W, H = find_daily_pattern_nmf(data)
    
    plots = [W.T[0] * H.mean()]

    plt.plot(plots[-1])

    plt.xlabel("Hours (24h)")
    plt.ylabel(species + " level (ug/m3)")
    return plots   

# Read in a csv from GUIapi and simplify the column names
def load_london_air_data(filename):
    data = pd.read_csv(filename)

    # Use simpler names
    new_names = {}
    for c in data.columns:
        for full_name, short_name in speciespair.items():
            if c.find(full_name) != -1:
                new_names[c] = short_name

    data.rename(index=str, columns=new_names,inplace=True)

    return data

# Get some information about missing data
def data_integrity(data, species):
    missing = data[data[species].isna()]["MeasurementDateGMT"]

    missing_entries = len(missing.index)
    total_entries = len(data.index) - missing_entries
    
    missing = missing.str[0:11].unique()
    missing_days = missing.size

    missing_per_month = [0]*12

    for d in missing:
        missing_per_month[int(d[5:7]) - 1] += 1

    print(missing_entries, " missing entries")
    print(missing_days, " days missing")
    print(missing_per_month, " missing per month")
 

# Load in the database
db = sqlite3.connect("data/london_air_db")